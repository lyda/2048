#!/bin/bash
#
# build.sh
# Copyright (C) 2018 Kevin Lyda <kevin@phrye.com>
#
# Distributed under terms of the GPL license.

gem install sass
mkdir public
cp favicon.ico public
cp index.html public
cp -a js public/js
cp -a meta public/meta
cp -a style public/style

#sass --unix-newlines style/main.scss > public/style/main.css
